---
layout: post
title:  "Blog Engineering"
date:   2019-05-22 21:40:18 +0000
categories: misc
---

So, I promised a while ago I would do a post about how I'm actually running the infrastructure for this site, and to be honest it's nothing special. However, I'll go over it anyways because I found it to be a fun project. 

# Hosting

I already had an account with [Digital Ocean](https://m.do.co/c/c00e376be3ee) (Shameless use of an affiliate link there) So naturally I decided to go with them for hosting the [CentOS](https://www.centos.org/) VPS I used for a base. I chose CentOS for the purposes of stability and security. Not to say a debian based system like Ubuntu isn't those things, just that I tend to trust the [SELinux](https://selinuxproject.org/page/Main_Page) defaults in CentOS more than I trust the [AppArmour](https://wiki.ubuntu.com/AppArmor) defaults in Ubuntu. 

I tend to prefer CentOS also because of the simplicity, Ubuntu seems to come with a lot of different things that I never end up using, of course this varies based on what [task](https://help.ubuntu.com/community/Tasksel#Tasks_List) you decide to use at install. But, I feel like a bare install of CentOS is just lighter.

Ubuntu in my opinion is very good at delivering a desktop GNU/Linux experience, while CentOS seems to focus more on the server side of things. 

I'm actually as of this post still using Ubuntu on both my laptop and my desktop (This will change in the future I'm sure). 



# Software
Alright, enough rambling. Let's get on to some of the more nitty-gritty of the software rendering the static page you see today. On the backend I'm using [Jekyll](https://jekyllrb.com/) to do all of the fancy stuff, Jekyll actually seemed like a perfectally logical choice to me due to the fact it allows me to author my posts using markdown and a simple file structure. 

I didn't see the need to use something like [Wordpress](https://wordpress.org/download/) or [Drupal](https://www.drupal.org/), as I just wanted something easy to manage. 

A database is an extra layer of complexity and resources I just didn't need running. I think both of the aforementioned tools are fine in certain usecases, but for a simple static blog or website Jekyll works just fine. Besides, Wordpress doesn't have the added benefit of writing your posts in vim. 

In front of Jekyll I'm running an [NGINX](https://nginx.org/en/) reverse proxy to handle pesky things like SSL and redirects. I really like NGINX for this kind of use-case. I'm sure it works fine for actual web-servery stuff, But when stacked up against the same functionality of [Apache](https://httpd.apache.org/) when it comes to proxying I feel like NGINX wins by a large margin. Nginx has a much simplier configuration structure as well as a smaller ram footprint. 

Apache was the defacto standard for years, but NGINX really seems to have gained so much traction over it in such a short time. It just goes to show how fast technology evolves. 

## "The new hotness won't always be so hot." 

Looking at things through this lens I really wonder if we're going to look at something like [GO](https://golang.org/) or [Rust](https://www.rust-lang.org/) in the future the same way we look at Java today. Will Android one day have the same stigma carried as [Symbian](https://en.wikipedia.org/wiki/Symbian) or any other mobile phone OS does now, Or will it be remembered for what it brought to the table? i


We humans really tend to either look at the past with blissful nostalgia or disdainful memories. 

No real inbetween for the societal conciousness. 

# Posting

I'm currently writing my posts Via SSH and vim. I see this as a kind of security flaw though, I try to limit the time that port 22 is open to only when I'm writing, but I feel like that still isn't the most secure. I could move over to a [port knocking](http://portknocking.org/) model, but I decided that I'm going to eventually move to using git to manage my blogs theme and posts since Jekyll is mostly comprised of flat files. This pull model allows me to only expose 80 and 443 on the server. I like the peace of mind that this provides me security wise, there's still attack vectors on those ports, but with decent firewall rules and consistent updates the site should remain fairly secure.  

# Wrapping things up 

I know I haven't really been active on here much, I've been super busy with work and some of my other side projects (Which I hope to feature here when I make more progress.), But I'm gonna try to start posting more than once or twice a month. I've even reached out to a few friends to write guest posts, so look forward to seeing at least one guest post in the near future. 

That's all I really have for now. 

Until then, 

Thanks for reading.    

~ps3udonym

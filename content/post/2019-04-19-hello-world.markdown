---
layout: post
title:  "Hello World!"
date:   2019-04-19 03:30:18 +0000
categories: misc
---

# Who am I?
This is my first post I guess. I should probably start by introducing myself. 

1. I have a passion for FOSS/Libre software 
2. I am a huge advocate for privacy both online and offline
3. I believe information should be free 

# Why am I doing this? 
I want to use this platform to share my ideas. 
These ideas could be manifested as simple posts about what's going on in my life, or maybe something interesting I found/did while working on my numerous projects. 
I will also probably discuss current events I have reflections on. 
I don't really consider myself an authority on anything, but I hope I can provide meaningful content to an internet now filled with more signal than noise.

# What's to come?
That's all I can really think of for right now, but I intend to be back in the next couple days with some more posts, I am planning on using my first real post to document how I built out this blog to begin with. 



Thanks for reading!

~ps3udonym

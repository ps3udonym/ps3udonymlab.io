---
title: "Blog (Re)engineering"
date: 2019-09-09T23:05:27-07:00
---

So, It's been a while, 

As you may have noticed, things are looking different around here. That's because I've realized that my previous situation was mildly untenable when it came to running my blog. You see, there is a list of issues I was having with migration of hosting providers and redeploying jekyll to suit my needs. I will shamefully admit that I was previously using the defualt jekyll theme for my blog on account of me not fully understanding how jekyll themes worked. (To this day, I am still confused by the process...) Now maybe it's because I'm lazy, or maybe it's because I wanted to try something new, but I decided I wanted to redeploy my blog over to gitlab pages to reduce cost. Initially I tried redeploying jekyll to GL pages, but I ran into the same issues I was having when I failed migrating the blog over to vultr. (Ruby dependancy issues, jekyll puking obscure errors, etc...). So I decided that I would try to utilize hugo for the purpose. As yo ucan see the process ended up being a lot smoother and looks pretty snappy to boot. You might still be wondering why I made the choices I did so I will drop a table below comparing the old and the new setup to illustrate my logic. 

| Reason | New | Old |
|------------------------------------|------------------------------------------|---------------------------------------------------------------------------------------|
| Cost | Free | $5 a month |
| Content Authoring | Anywhere in the world via git | Restricted to trusted machines connecting to the server over SSH |
| Platform | Hugo | Jekyll |
| Time spent Managing Infrastructure | Little to none (gitlab does this for me) | at least 5 or 6 hours a month worrying about patches, SW updates and fighting SElinux |

As you can see there are numerous benefits to the new setup. Gitlab will even do me the solid of allowing me to use a custom domain name for the site, as well as allowing me to bring my own SSL certs. As another bonus I've learned some pretty neat tricks in this process involving docker and gitlab CI. Now I have a better understanding of the process gitlab uses I will likely experiment with doing the page generation on my own hardware in the near future instead of wasting their precious runner minutes. 

This was kind of a short post, but I just wanted to leave an update talking about how I reconfigured the blog and skim a little bit on the technical aspects. Now that publishing is more of an agile process I think I should be able to post here more often.

Thanks for reading. 

~Ps3udonym

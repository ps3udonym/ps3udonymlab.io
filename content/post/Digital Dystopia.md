---
title: "Digital Dystopia"
date: 2020-06-22T08:44:14-07:00
draft: false
---



Every day we descend further into a cyberpunk dystopia that not even William Gibson could've predicted. We're all gears in a giant fucking machine that reduce our unique qualities as people into a neatly packed bundle of information that is sold and shipped off to advertisers hundreds of times a day. Everyone was so concerned about living in some dystopia where you get reduced to a number. The ironic part is that number ended up being an advertising GUID rather than your SSN. Get me outta of this cyberpunk hell man. 

I'm tired of seeing society bend to the whims of Bezos and Zuckerberg. I'm tired of living in a world where people go in tho the tens of thousands of dollars of debt for internet points. Can you imagine how things are going to proceed? As VR gets more affordable the advertisers will start fabricating a new reality for each user rather than influencing the one they currently live in. I don't think it's crazy to assume that in that era we'll see homeless VR junkies living out fantasy lives through their Google Cardboard units in the middle of the street. Reduced to living in the unreality that is the adsphere

 (premium access to the adsphere can be purchased for just the low low price of $9.99 a month! All other users can still enjoy the riveting world of the adsphere with the slight modification of the reality they inhabit driven by our generous sponsors.) 

If you haven't seen minority report, there's a scene where Tom cruise is running through a mall and all of the advertisements are targeted directly at him. This was a spooky gag in that movie from the early 2000s, but it's a reality now. 

Facebook and google are running your data into predictive algorithms to understand what product you're gonna want to buy next. And with such a rich amount of metadata, it's arguable that they're creating a sort of digital clone that lives in this even worse dystopia where it's only existence is viewing ads. Thank God that it doesn't have a conscious because that would be entirely heartbreaking. 

Sorry if this comes off as overly wordy, I havent slept well in the last couple of days and felt the need to vent ...

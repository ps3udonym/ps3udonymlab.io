---
title: "Threat Modeling Your Life (PART 1)"
date: 2020-07-30T13:45:12-07:00
draft: false
---
# Intro
So I recently was asked by some friends about securing your personal life from ad agencies and possibly the government. Well I can provide steps to negate the privacy violations of AD companies, I cannot give anyone a silver bullet solution to hiding from the government. I can provide some steps that might make their lives harder, I cannot guarantee any of the below steps will make it impossible for them to access your data. You might be curious how I can decide that, to get a full understanding I need to first describe threat modeling. 

# Threat Modeling
What Is threat Modeling? According to google: _Threat modeling is the practice of identifying and prioritizing potential threats and security mitigations to protect something of value, such as confidential data or intellectual property._ I will say that's a fairly accurate description. So to begin this journey you need to first decide what information you're looking to protect, this is likely an attribute of yourself. I won't try to enumerate what that might be, because you already know. So in order to to threat model we need to walk through some steps that will ask questions about your sensitive information. 

## Threat Evaluation Questions

1. Is the information you're trying to protect already available on the internet?(If you are unsure I will be providing steps on how to "dox" yourself further down)
2. Is the information you're trying to protect linked to a behaviour of yours? (For example, if you subscribe to a specific political ideology you might post/say things that will make this obvious)
3. How much are you willing to give up in context of services you currently use? (Facebook, Twitter, FB Messenger, What's APP, Google maps, GMAIL, Etc...)
4. How much do you trust the devices you use? Is your phone hardened/encrypted? What about your laptop? 

## Pros and Cons
Being well hidden will always come at a cost. There are going to be sacrifices you will have to make in order to hide your secrets, these may be services or routines that make your life convenient. You might even have to change the hardware you're using. 

# Actually Securing your digital footprint
In this section we're going to start enumerating different ways entities spy on you and how to mitigate those methods of spying. I am going to start at the lowest level which is the devices you use on a daily basis and the information they collect on you. 

## Phones

I won't go into a guide based on a specific manufacturer, but I will generally go over what data the people who make your phone potentially have access to when you run it without modification. This data includes, but is not limited to the following:

* Contents of text messages
* Metadata surrounding phone calls
* Location data
* Things said to your virtual assistant (Google, Siri or Cortana)

It's also safe to assume the vendor who developed your phones Operating System (OS) also has access to this data (Apple, Google, Microsoft). I won't go into how to prove this as it would get very technical very fast, but if you are curious you can start investigating that on google fairly easily. 

Moral of the story is that if you don't own the software or the firmware you must assume that data is compromised. 

So, how do you prevent this data collection? You have to deplatform the people who are spying on you. How do you do this? This is done by replacing their OS embedded with potential spyware with a known safe OS. And if you're willing to go that route you might as well upgrade your hardware to something that supports such a transition. There are 2 ways to tackle this, one way would be to purchase a phone already running audited and known to be "safe" OS. Or you could buy off the shelf hardware that supports you flashing a known safe OS to it. 

### Buying
Here are a few places you can buy pre-flashed phones that are known to be privacy respecting. 

* https://esolutions.shop/
* https://tehnoetic.com/mobile-devices
* https://store.pine64.org/product/pinephone-community-edition-postmarketos-limited-edition-linux-smartphone/

### DIY
This option might be cheaper, but requires some technical knowhow and time invested. 

So in order to flash a rom you will need a device to flash to, I can recommend devices put out by **google** and **oneplus**, I do NOT recommend running their stock OS as it is assumed to be filled with spyware, however they ship with unlocked bootloaders which make life a lot easier for flashing. As for the process this varies from phone to phone. But the steps are very similar, so I'll leave a generic set of steps here so you know what to respect

1. Back up any piece of important data you have on your phone
2. Follow the manufacturers process to unlock your phone's bootloader 
3. Plug your phone into your PC and flash a FOSS bootloader such as TWRP and follow the process as guided until it's completed
4. boot into your new bootloader
5. begin with the process of flashing your OS and any system apps you want. (Make sure the ROM you chose does not ship with google apps as this defeats the purpose)
6. Enjoy your newly liberated phone. 

## Your apps
Many phone apps are also a potential threat to your security, If you want to be truly free you have to evaluate the apps you use as well, I will post a link below that will list out some popular apps and their "free" alternatives. Most of these apps will likely be on an alternative app store called F-Droid. Installing it is as easy as going to the F-Droid Website and installing the APK they provide (Don't worry the process is super simple and google will help you out)

* [F-Droid Website](https://f-droid.org/)
* [Alternative "free" apps](https://prism-break.org/en/categories/android/)

## Your connectivity
Outside of hardening the security at the lower levels of your phone, you also need to harden how your communications are done across the 'wire'. There are a wide amount of applications and services that support End to End Encryption (E2E) I can recommend the following for IM style communications. 

* [Signal](https://signal.org/en/)
* Self Hosted OTR XMPP 
* Encrypted IRC chat
* [Telegram (Secret Chats only)](https://telegram.org/)
* [Briar](https://briarproject.org/)

When it comes to email I would recommend finding a privacy concious email host/provider and using them for any personal affairs (Feel free to use your GMAIL or HOTMAIL email for promotional garbage). I can recommend the following EMail hosting services. 

* [Riseup (Also provides encrpyted/OTR XMPP as mentioned earlier)](https://riseup.net/en/email)
* [cock.li (Vince is a pretty cool guy who doesn't afraid of anything)](https://cock.li)
* [Lavabit](https://lavabit.com)

While these providers may claim to respect your privacy it is recommended to still assume they are also watching your email, therefore if you need to have secure communications it is recommended to use GPG to encrypt your emails before they are sent to friends. I will post a guide below describing how to use GPG to encrypt emails end to end. 

https://www.liquidweb.com/kb/how-do-i-use-gpg/ (This is a linux specific guide, but I will get into why using linux is a better option in the laptop/desktop section)

# Conclusion

This is just part 1, I am releasing this now until I can get the resources together to write a part 2 which will go over doxing yourself and securing your laptop/desktop. 


Until Then, 

Thanks for reading,

Ps3udonym

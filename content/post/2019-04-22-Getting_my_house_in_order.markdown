---
layout: post
title:  "Getting My house in order"
date:   2019-04-22 19:50:18 +0000
categories: misc
---

Many people might be familiar with the phrase "Getting your house in order" If I remember correctly it's derived from 2 Kings 20:1 in the Bible, Although not religious, I believe that it is a good practice to live by. Over the past weekend I spent time getting my digital house in order. In sorts, I'm a bit of a nomad as I "live" mostly on my thinkpad T420 and spend a great deal of time distrohopping. 

I most recently was running Fedora 29 and ended up getting frustrated with the philosophy in which packages are managed. I found myself with piles of errors from abandoned user repos on [COPR](https://copr.fedorainfracloud.org/). which brought me mismatched versions and conflicts abound. As a result I decided Sunday night I was going to experiment with [Frugalware](https://www.frugalware.org/) which is in a way slackware derived and uses a forked version of Arch's pacman for package management. 

I was able to intall it fairly quickly, but I was soon plagued with issues with trackpad drivers and spanish ending up as my defualt languange (likely an error on my part) After I sorted out my two initial issues I finally connected to my wifi and ran updates as is customary when installing a new OS. This was a mistake. As soon as I booted after restarting post-update I had found that wayland was installed over x11 which caused instability in GDM. as well as my wifi driver package being overwritten with a different one (basically making my wireless card inoperable). Aside from these Issues there were conflicting packages trying to take ownership of the same files. and to top it all off I had one hell of a time trying to find any kind of documentation.

As a result of all of this I decided to call mercy and install the latest release of ubuntu (19.04). Since my laptop was in a non-workable state and I refused to pull my lazy ass off of the couch I went the route of utilizing [DriveDroid](https://www.drivedroid.io/) to boot an ISO of Disco Dingo. The install went flawlessly and I was up and running again within an hour. 

I'm not a huge fan of Ubuntu. Honestly in my opinion it has similar problems to Fedora, but not as many problems as something like Frugalware. However, I will say it's stable. It's kind of like going back to your parents house for a weekend after a messy breakup, It's not the most amazing place in the world to be, but It'll always be comfortable and a place of refuge. When things just go south you can always go back home and get yourself straightened out until you're ready to go back to the day to day.

Cut to this afternoon, I was reading [HN](https://news.ycombinator.com/) and stumbled upon a project called [Rigolith](https://regolith-linux.org/) which is an Ubuntu based distro with an interesting concept, take the stability of Ubuntu and give it a cleaner more "minimalist" interface. This interface is basically i3 with some modifications to make it look more attractive. I usually end modifying the absolute crap out of i3 on my own anyways, so I thought I'd give it a try and install the PPA and desktop package supplid by the mantainer on top of my freshly minted ubuntu deployment. So far I'm pretty impressed. I don't think the default colorscheme works the best with my older T420 display, but I can play with that later. 

Onto the other half of the house, I have an aging gaming desktop running Solus, I used to be an evangelist for Solus, but it really hasn't been the same since Ikey left the project. I'm debating weather or not to install ubuntu on my desktop as well, however, I'm also tempted to turn it and it's many drivebays into a second virtualization server for my home network. I think I could have a guest with full access to the GTX 1060 it has via IOMMU passthrough and use that guest for things like hash cracking and messing around with other workloads that could benefit from the spare CUDA cores. then use the rest of the dormant processing power for other "utility" VM's. I'd likely achieve all of this by using CentOS + KVM + [Kimchi](https://github.com/kimchi-project/kimchi). But this is just an idea for now. This ties into my itch of wanting a portable solution for my hobby work. I can very affordably get a dock for my laptop and I already have a decent desktop setup. It'd be trivial to make the switch.

I know this post was a bit lengthy and ranty, but I promise I'll be getting to the good stuff soon.

Thanks for reading.    

~ps3udonym

---
title: "Auto Iso"
date: 2020-04-05T01:22:30-07:00
draft: false
---

A while back I was bored and decided to start messing around with technical oddities (as I tend to) when I came across the fact that GRUB can load an ISO on an existing file system as a boot option. When I figued this out I immediately started coming up with ideas of how I could best use this awesome feature. The first thing that came to mind was to replace the bootable kali thumbdrive on my keychain with an ISO stored on my laptop. I thought it was a great idea. But then I realised there was potential to do so much more than just loading the same image over and over. 

You see, Kali has a weekly build that ships out with the latest and coolest features, and if I'm booting from a file, why not just go that route? So I set forth to write a bash script to do the work of sorting out and downloading the lastest weekly build of kali for me with the same name every time and a verified checksum. 

In this post I will go over both my grub configuration and the script I wrote so that you can repeat this yourself (if you're so inclined). I should also note that this process can be done with almost any operating system that supports live booting via grub, with a few modifications this could easily also used in other cases. For example, assume you use Use Arch or Ubuntu at home on your own wifi and all of your secrets live in your encrypted home directory, with this process you could keep a live image of something like QUBES handy on your HDD for when you go somewhere and have to connect to a network you don't trust without potentially opening up your sensitive data to compromise. I know it sounds like paranoia, but it's just an example of how this technique could be used in a way to defend your privacy.

Examples aside, let's get into the technical stuff.


## The Technical Stuff

To start, we're going to need to find a directory for our ISO(s) to live once we download them (I put them in `/opt/isos`). Inside that directory there are 2 more directories `verified` and `unverified` Unverified is used for staging until we can verify the checksum, which if it is verified successfully it will be moved to the verified directory. I am embedding a gitlab snippet of the script I mentioned below to give an overview of this process. 

### Script

```
#!/bin/bash

# iso_type can be either live or installer (live is best for direct booting from grub)
iso_type=live

#arch can be either amd64 or i386
arch=amd64

# Site that has the weekly build of kali (this is the official mirror, it can be changed if you do not trust this source)
kali_repo="http://kali.download/kali-images/kali-weekly"

# Directory where the ISO will be staged to be verified 
unverified_dir=/opt/isos/unverified

# Directory where ISO will be placed if the hash is accurate
verified_dir=/opt/isos/verified

# Gets the proper hash for the kali ISO
hash=$(curl --user-agent 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36'  $kali_repo/SHA256SUMS | grep $iso_type | grep $arch | awk '{print $1}' )

# This could probably be refactored into a shorter awk command, but I'm lazy and bad at regex
iso_name=$(curl --location-trusted http://kali.download/kali-images/kali-weekly | grep iso | grep $arch | grep $iso_type | cut -d \" -f2)

# Cleans up unverified/bad ISO's from unverified directory to prevent any possible issues having those files there may cause (This is mainly out of my personal paranoia)
rm $unverified_dir/*

# Using wget here because it was faster than curl in my tests, feel free to change it to a curl solution if you'd like. 
wget --user-agent="Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/63.0.3239.132 Safari/537.36" --show-progress -O "${unverified_dir}/kali.iso" "$kali_repo/$iso_name"  

#Getting the hash of the ISO
isohash=$(sha256sum ${unverified_dir}/kali.iso | awk '{print $1}') 

# Verifies the hash and moves the ISO id the hash is accuate
if [[ $hash == "$isohash" ]]; then 
	cp ${unverified_dir}/kali.iso  ${verified_dir}/kali.iso && echo "Hash successfully verified, moving ISO to $verified_dir" 
else
	echo  "Unable to verify checksum of ISO, assuming compromised and ABORTING."
fi
```

Pretty neat huh? Now that we have that taken care of we need to modify our grub to boot the ISO from the menu, Where you modify this will depend on your Operating system so take care to google to find which file you'll need to modify so your changes wont get clobbered by an update. In most distros it looks like you can modify `/etc/grub.d/40_custom` without issue, (but again do your homework before you start modifiying stuff just to be sure). I'm going to put my grub snippet below so you can copy it and change it to fit your use case.

### Grub configuration

```
menuentry '<YOUR OS HERE>' {
  set gfxpayload=text # ~= vga='normal'
  set isofile_abspath='<YOUR ISOPATH HERE>'
  set isofile_devpath="${devroot}${isofile_abspath}"
  loopback loop "(${root})${isofile_abspath}"
  linux '(loop)/live/vmlinuz' boot='live' union='overlay' username='<LIVE USER HERE>' config components noswap noeject toram='filesystem.squashfs' ip='' nosplash findiso="${isofile_abspath}"
  initrd '(loop)/live/initrd.img'
}
```

With the proper modifications you should be able to use that snippet for most distributions of linux, you may also need to modify the path for `vmlinuz` and `squashfs` but those settings should work as default for most debian based distros (If you aren't using something with a debin base you can often check the grub file in the ISO for and idea of where these files are). Once you update this file you should be able to regenrate your grub through the usual means, for me it's `sudo update-grub`. After you update your grub file you sould be good to reboot and choose your new boot option in the grub menu. 

If you run into any issues in this process, check the about page of this blog and shoot me an email and I'll be happy to lend a hand. 

Happy Hacking! 

~Ps3udonym

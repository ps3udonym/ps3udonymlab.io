---
title: "On Tracking Thieves"
date: 2020-01-01T15:34:34-07:00
---

Last night one of my neighbors recorded video of some hooligans running around checking car doors in hope of finding some "loot". Incensed by this I have hatched a plan, and it looks like this. 

# My Evil plan to track thieves...

1. Set up an inexpensive wireless access point on a a separate VLAN of my network (rate-limited to the point of being useless of course...)
2. Broadcast as many common SSIDs as I can think of (GoogleStarbucks, ATTWIFI, etc...). In theory modern smartphones will automatically try to connect to these "familiar" SSIDs. 
3. Once the "smart" devices of the would be thieves connect to my access point I can load some custom JS on to their devices via a captive portal. 

This theoretical snippet should be able to call location services either via social engineering methodology or clandestinely if the operating system running on the devices is cooperative. I'd imagine  it'd look something like the snippet below to get their location on a consistent eventloop constantly polling back to whatever server I stood up to collect this data with a GUID to track each device. 
 
```
navigator.geolocation.getCurrentPosition(

    // Success callback
    function(position) {


        position = {
            coords: {
                latitude
                longitude
                accuracy 
            }
            timestamp
        }

    },

    }
);
```

4. As soon as I catch wind from my neighbors of this happening again I can watch timestamps for new devices that have connected in the timeframe  of the incident and  watch the location of that device via the GUID. Once I am certain of my target I can report their approximate location to the police so that they can investigate.  

I have no idea if this will play out exactly as I'm assuming, but from what I do it seems like a sound concept in my head. I will have to do testing and update this post if It's successful. 

I guess if there was a moral to this story it would be `Don't fuck around in the neighborhoods of bored hackers...`

~Ps3udonym
